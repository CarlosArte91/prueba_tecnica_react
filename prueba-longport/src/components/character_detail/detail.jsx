import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import getCharacterById from "../../helpers/getCharacterById";

function Detail() {
    const [character, setCharacter] = useState({});
    const { id } = useParams();

    console.log(character);

    useEffect(() => {
        const getId = async () => {
            setCharacter(await getCharacterById(id));
        };
        getId();
    }, []);

    return(
        <>
        {
            character.name ?
            <div>
                <h2>{character.name}</h2>
                <img src={`${character.thumbnail.path}.${character.thumbnail.extension}`} alt={character.name} />
                <div>
                    <h3>Comics</h3>
                    {
                        character.comics.items.map(comic => {
                            return(
                                <p>{comic.name}</p>
                            )
                        })
                    }
                </div>
                <div>
                    <h3>Events</h3>
                    {
                        character.events.items.map(event => {
                            return(
                                <p>{event.name}</p>
                            )
                        })
                    }
                </div>

            </div>
            : <span>No hay personaje</span>
        }
        </>
    );
}

export default Detail;