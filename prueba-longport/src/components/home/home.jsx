import Characters from "../all_characters/characters";
import Navbar from "../navbar/navbar";
import Paginate from "../paginate/paginate";
import styles from "./home.module.css";

function Home() {
    return (
        <div className={styles.container}>
            <Navbar/>
            <Characters/>
            <Paginate/>
        </div>
    );
};

export default Home;