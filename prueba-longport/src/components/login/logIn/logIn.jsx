import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import styles from "./logIn.module.css";

function LogIn() {
    const navigate = useNavigate();
    const session = localStorage;
    const [user, setUser] = useState({});

    console.log(user);

    const handleInputs = (event) => {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });        
    };

    const submitData = (event) => {
        event.preventDefault();
        if (user.email === session.email && user.password === session.password) {
            alert(`Bienvenido ${session.name} ${session.lastName}`);
            navigate("/home");
        }
        else {
            alert("Los datos no son validos");
            setUser({});
            navigate("/login")
        }
    };

    return (
        <div className={styles.container}>
            <h3>Inicio de sesión</h3>
            <form className={styles.form_} onSubmit={submitData}>
                <input onChange={handleInputs} type="text" placeholder="Correo" name="email" />
                <input onChange={handleInputs} type="password" placeholder="Contraseña" name="password"/>
                <div>
                    <span>¿No eres usuario?,</span>
                    <Link to={"/signup"}>
                        <span>Registrate</span>
                    </Link>
                </div>
                <input className={styles.button} type="submit" value="Ingresar" />
            </form>
        </div>
    );
};

export default LogIn;