import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import styles from "./signUp.module.css";

function SignUp() {
    const navigate = useNavigate();
    const [user, setUser] = useState({});

    console.log(user);

    const handleInputs = (event) => {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });        
    };

    const submitData = (event) => {
        event.preventDefault();
        console.log("enviando");
        for (const item in user) {
            localStorage.setItem(item, user[item]);
            sessionStorage.setItem(item, user[item]);    
        }
        alert("Usuario creado");
        navigate("/home");
    };

    return (
        <div className={styles.container}>
            <h3>Crear cuenta</h3>
            <form className={styles.form_} onSubmit={submitData}>
                <input onChange={handleInputs} type="text" placeholder="Nombre" name="name" />
                <input onChange={handleInputs} type="text" placeholder="Apellido" name="lastName" />
                <input onChange={handleInputs} type="email" placeholder="Correo" name="email" />
                <input onChange={handleInputs} type="password" placeholder="Contraseña" name="password"/>
                <div>
                    <span>¿Ya tienes cuenta?,</span>
                    <Link to={"/login"}>
                        <span>Inicia sesión</span>
                    </Link>
                </div>
                <input className={styles.button} type="submit" value="Crear" />
            </form>
        </div>
    );
};

export default SignUp;