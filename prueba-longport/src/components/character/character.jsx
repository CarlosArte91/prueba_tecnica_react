import { Link } from "react-router-dom";
import styles from "./character.module.css";

function Character({ id, name, image }) {
    return (
        <>
        <Link to={`/detail/${id}`}>
            <div className={styles.container}>
                <span>{name}</span>
                <img src={image} alt={name} />
            </div>
        </Link>
        </>
    );
};

export default Character;