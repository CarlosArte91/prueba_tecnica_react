import { useContext } from "react";
import PaginateContext from "../../context/paginateContext";
import styles from "./paginate.module.css";

function Paginate() {
    const { page, handleDecrement, handleIncrement } = useContext(PaginateContext);
    return (
        <div className={styles.container}>
            <input type="button" value="<" onClick={handleDecrement}/>
            <span>{page}</span>
            <input type="button" value=">" onClick={handleIncrement}/>
        </div>
    );
};

export default Paginate;
