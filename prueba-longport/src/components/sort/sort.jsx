import { useContext } from "react";
import SortContext from "../../context/sortContext";
import styles from "./sort.module.css";

function Sort() {
    const { sort, handleSort } = useContext(SortContext);
    return (
        <div className={styles.container}>
            <span>Elige un orden</span>
            <button value="ascendant" onClick={handleSort}>Ascendente</button>
            <button value="descendant" onClick={handleSort}>Descendente</button>
        </div>
    );
};

export default Sort;