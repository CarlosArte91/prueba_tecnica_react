import NumberRows from "../numberRows/numberRows";
import Sort from "../sort/sort";
import styles from "./navbar.module.css";

function Navbar() {
    return (
        <div className={styles.container}>
            <Sort/>
            <NumberRows/>
        </div>
    );
};

export default Navbar;