import { useContext, useEffect, useState } from "react";
import Character from "../character/character";
import getCharacters from "../../helpers/getCharacters";
import SortContext from "../../context/sortContext";
import sortCharacters from "../../helpers/sortCharacters";
import PaginateContext from "../../context/paginateContext";
import styles from "./characters.module.css";

function Characters () {
    const { sort } = useContext(SortContext);
    const { page, charactersXPage } = useContext(PaginateContext);
    const [arrayCharacters, setArrayCharacters] = useState([]);

    useEffect(() => {
        const getApi = async () => {
            setArrayCharacters(
                sortCharacters(await getCharacters(), sort)                              
            );
        };
        getApi();        
    }, [sort]);

    return (
        <div className={styles.container}>
            {
                arrayCharacters.length ? arrayCharacters.slice(
                    (page - 1) * charactersXPage,
                    (page - 1) * charactersXPage + charactersXPage
                ).map(character => {
                    return (
                        <Character
                            id={character.id}
                            name={character.name}
                            image={`${character.thumbnail.path}.${character.thumbnail.extension}`}
                            key={character.id}
                        />
                    );
                }) : <span>No hay personajes</span>
            }
        </div>
    );
};

export default Characters;
