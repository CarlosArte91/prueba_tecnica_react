import { Link } from "react-router-dom";
import styles from "./landing.module.css";

function Landing() {
    return (
        <div className={styles.container}>
            <h1>Personajes de Marvel</h1>
            <Link to={"/login"}>
                <div className={styles.button}>Iniciar sesión</div>
            </Link>
        </div>
    );
}

export default Landing;