import { useContext } from "react";
import PaginateContext from "../../context/paginateContext";
import styles from "./numberRows.module.css";

function NumberRows() {
    const { charactersXRow, handleDecrementRows, handleIncrementRows } = useContext(PaginateContext);

    return (
        <div className={styles.container}>
            <span>Elige el número de filas</span>
            <input type="button" value="<" onClick={handleDecrementRows}/>
            <span>{charactersXRow}</span>
            <input type="button" value=">" onClick={handleIncrementRows}/>
        </div>
    );
};

export default NumberRows;