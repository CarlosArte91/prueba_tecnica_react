
const sortCharacters = (array, sort) => {
    return array.sort((a, b) => {
        return sort === "ascendant" ? 1 : -1
    });
};

export default sortCharacters;