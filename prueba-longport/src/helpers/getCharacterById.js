import axios from "axios";
import { base, apiKey } from "./urlAPI.js";

const getCharacterById = async (id) => {
    try {
        const { data } = (await axios.get(`${base}${id}${apiKey}`));
        return data.data.results[0];
    } catch (error) {
        console.log(error);
    }
};

export default getCharacterById;