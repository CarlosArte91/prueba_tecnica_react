import axios from "axios";
import { allCharact } from "./urlAPI.js";

const getCharacters = async () => {
    try {
        const { data } = (await axios.get(allCharact));
        return data.data.results;
    } catch (error) {
        console.log(error);
    }
};

export default getCharacters;