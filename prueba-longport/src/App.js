import { Navigate, Route, Routes } from 'react-router-dom';
import './App.css';
import Detail from './components/character_detail/detail';
import Home from './components/home/home';
import Landing from './components/landing/landing';
import LogIn from './components/login/logIn/logIn';
import SignUp from './components/login/signUp/signUp';
import { PaginateProvider } from './context/paginateContext';
import { SortProvider } from './context/sortContext';

function App() {
  return (
    <div className="App">      
      <PaginateProvider>
        <SortProvider>
          <Routes>
            <Route path='/home' element={<Home/>}/>
            <Route path='/detail/:id' element={<Detail/>}/>
            <Route path='/' element={<Landing/>}/>
            <Route path='/login' element={<LogIn/>}/>
            <Route path='/signup' element={<SignUp/>}/>
            <Route path='*' element={<Navigate to={"/home"}/>}/>
          </Routes>
        </SortProvider>          
      </PaginateProvider>   
    </div>
  );
}

export default App;