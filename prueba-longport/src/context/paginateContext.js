import { createContext, useState } from "react";

const PaginateContext = createContext();

const PaginateProvider = ({ children }) => {
    const elements = 20;
    const [page, setPage] = useState(1);
    const [charactersXRow, setCharactersXRow] = useState(1);
    const [charactersXPage, setCharactersXPage] = useState(3);

    const handleDecrement = () => {
        if (page > 1) setPage(page - 1);
    };

    const handleIncrement = () => {
        if (page < Math.ceil(elements / charactersXPage)) setPage(page + 1);
    };

    const handleDecrementRows = () => {
        if (charactersXRow > 1) {
            setCharactersXRow(charactersXRow - 1);
            setCharactersXPage((charactersXRow - 1) * 3);
        };
    };

    const handleIncrementRows = () => {        
        if (charactersXRow < 7) {
            setCharactersXRow(charactersXRow + 1);
            setCharactersXPage((charactersXRow + 1) * 3);
        };
    };

    const data = {
        page,
        setPage,
        handleDecrement,
        handleIncrement,
        charactersXRow,
        handleDecrementRows,
        handleIncrementRows,
        charactersXPage,
        setCharactersXPage
    };

    return <PaginateContext.Provider value={data}>{children}</PaginateContext.Provider>
};

export { PaginateProvider };
export default PaginateContext;