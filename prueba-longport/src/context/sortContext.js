import { createContext, useContext, useState } from "react";
import PaginateContext from "./paginateContext";

const SortContext = createContext();
const initialSort = "ascendant";

const SortProvider = ({ children }) => {
    const { page, setPage } = useContext(PaginateContext);
    const [sort, setSort] = useState(initialSort);   

    const handleSort = (event) => {               
        if (event.target.value === "ascendant") setSort("ascendant");
        else setSort("descendant");
        if (page !== 1) setPage(1);       
    };

    const data = {sort, handleSort};

    return <SortContext.Provider value={data}>{children}</SortContext.Provider>
};

export { SortProvider };
export default SortContext;
